1) Just click on the + sign at the bottom of the screen.
2) Snap a yummy photo and upload it.
3) Enter basic information about the food to help others mouth water.
4) Set up your pick up location for delivery and maintain your availability.
5) ...And Share.
6) Once the food is delivered, just click on "Food Delivered" so that the listing is invisible to others.