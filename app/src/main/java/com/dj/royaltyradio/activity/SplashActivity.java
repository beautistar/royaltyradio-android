package com.dj.royaltyradio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.models.UserModel;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends BaseActivity {

    static final int SPLASH_DELAY = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String email = EasyPreference.with(SplashActivity.this).getString("easyEmail", "");
                login(email);
            }
        }, SPLASH_DELAY);
    }

    private void login(String email){
        if(email.length() == 0){
            Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        /// API call
        AndroidNetworking.post(Constants.SERVER + "logined")
                .addBodyParameter("email", email)
                .setTag(this)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String result = response.getString("result_code");
                            if(result.equals("0")){
                                JSONObject userObj = response.getJSONObject("user_info");
                                UserModel userModel = new UserModel();
                                userModel.setId(userObj.getInt("user_id"));
                                userModel.setUsername(userObj.getString("username"));
                                userModel.setEmail(userObj.getString("email"));
                                userModel.setPicture(userObj.getString("photo"));
                                userModel.setReport(userObj.getString("report"));
                                userModel.setBlock(userObj.getString("block"));
                                userModel.setIs_admin(userObj.getInt("is_admin"));
                                Commons.thisUser = userModel;

                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }else if ( result.equals("1")){
                                Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });

    }
}
