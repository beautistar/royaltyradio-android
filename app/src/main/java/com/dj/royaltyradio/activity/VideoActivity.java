package com.dj.royaltyradio.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.models.VideoModel;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class VideoActivity extends BaseActivity {

    LinearLayout linContainer;
    WebView webView;
    Firebase refVideo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        String Url = "https://iframe.dacast.com/b/140114/c/510510";

        webView = (WebView)findViewById(R.id.web);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);     // JavaScript Event Enable
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(Url);

        setupUI(findViewById(R.id.activity), this);
        ((ImageView)findViewById(R.id.imv_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //----------------- UI version test start ----------------------////
        /**********FireBase *************/

        Firebase.setAndroidContext(this);
        refVideo = new Firebase(Constants.FIREBASE_URL + "message/" + Constants.FIREBASE_ID_VIDEO);


        linContainer = (LinearLayout) findViewById(R.id.lin_container);

        ((ImageView)findViewById(R.id.imv_send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Commons.thisUser.getReport().equals("report")){
                    showToast("Sorry, you were reported.");
                    return;
                }
                if (Commons.thisUser.getBlock().equals("block")){
                    showToast("Sorry, you were blocked.");
                    return;
                }

                String message = ((EditText)findViewById(R.id.edit_write)).getText().toString().trim();
                ((EditText)findViewById(R.id.edit_write)).setText("");
                if (message.equals(""))
                    return;

                Map<String, String> map = new HashMap<String, String>();
                map.put("message", message);
                map.put("time", String.valueOf(new Date().getTime()));
                map.put("sender_id", String.valueOf(Commons.thisUser.getId()));
                map.put("name", Commons.thisUser.getUsername());
                map.put("email", Commons.thisUser.getEmail());
                map.put("photo", Commons.thisUser.getPicture());
                refVideo.push().setValue(map);
                Log.d("send====>", map.toString());
            }
        });

        refVideo.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String senderId = map.get("sender_id").toString();
                String name = map.get("name").toString();
                String photo = map.get("photo").toString();
                map.put("email", Commons.thisUser.getEmail());
                String time = map.get("time").toString();
                String key = dataSnapshot.getKey();

                if(senderId.equals(String.valueOf(Commons.thisUser.getId()))){
                    addMessageBox(senderId, message, time, photo, name, 1);
                } else {
                    addMessageBox(senderId, message, time, photo, name, 0);
                }
                Log.d("recevie====>", key);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    ////////// ********* Fire Base ********//////////
    public void addMessageBox(final String user_id, final String message, final String time, final String photoUrl, String name, int type){

        final LinearLayout cellLayout;
        if (type == 1)
        {
            cellLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_chat_me, null);
        } else {
            cellLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_chat, null);
        }
        Glide.with(getApplicationContext()).load(photoUrl).placeholder(R.drawable.avatar).into(((ImageView)cellLayout.findViewById(R.id.imv_user)));
        ((TextView)cellLayout.findViewById(R.id.tv_username)).setText(name);
        ((TextView)cellLayout.findViewById(R.id.tv_message)).setText(message);
        ((TextView)cellLayout.findViewById(R.id.tv_date)).setText(getCurrentTimeString(Long.parseLong(time)));
        if (Commons.thisUser.getIs_admin() == 1){
            cellLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View pinDialogView = LayoutInflater.from(VideoActivity.this).inflate(R.layout.alert_user_settings, null, false);
                    final androidx.appcompat.app.AlertDialog pinDialog = new androidx.appcompat.app.AlertDialog.Builder(VideoActivity.this).create();
                    pinDialog.setView(pinDialogView);

                    WindowManager.LayoutParams wmlp = pinDialog.getWindow().getAttributes();
                    wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                    /*wmlp.x = Gravity.CENTER - wmlp.width/2 - 20;   //x position*/
                    wmlp.y = 40;   //y position

                    TextView txvReport = (TextView) pinDialogView.findViewById(R.id.tv_report);
                    TextView txvBlock = (TextView) pinDialogView.findViewById(R.id.tv_block);
                    TextView txvDelete = (TextView) pinDialogView.findViewById(R.id.tv_delete);
                    TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
                    txvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pinDialog.hide();
                        }
                    });
                    txvReport.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            /// API call
                            AndroidNetworking.post(Constants.SERVER + "report")
                                    .addBodyParameter("user_id", user_id)
                                    .setTag(this)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                String result = response.getString("result_code");
                                                if (result.equals("0")) {
                                                    showToast("This user is reported.");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            // handle error
                                        }
                                    });

                            pinDialog.hide();
                        }
                    });
                    txvBlock.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            /// API call
                            AndroidNetworking.post(Constants.SERVER + "block")
                                    .addBodyParameter("user_id", user_id)
                                    .setTag(this)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                String result = response.getString("result_code");
                                                if (result.equals("0")) {
                                                    showToast("This user is blocked.");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            // handle error
                                        }
                                    });

                            pinDialog.hide();
                        }
                    });
                    txvDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            linContainer.removeView(cellLayout);

                            refVideo.orderByChild("time").equalTo(time).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        HashMap<String, Object> dataMap = (HashMap<String, Object>) dataSnapshot.getValue();
                                        for (String key : dataMap.keySet()){
                                            Object data = dataMap.get(key);
                                            try{
                                                HashMap<String, Object> requestData = (HashMap<String, Object>) data;

                                                dataSnapshot.getRef().child(key).removeValue();

                                            }catch (ClassCastException cce){
                                                // If the object can’t be casted into HashMap, it means that it is of type String.
                                                try{
                                                    String mString = String.valueOf(dataMap.get(key));
                                                    Log.d("datasnaptKey==>", mString);
                                                }catch (ClassCastException cce2){
                                                }
                                            }
                                        }
                                    }

                                }
                                @Override
                                public void onCancelled(FirebaseError firebaseError) {
                                    Log.d("User",firebaseError.getMessage() );
                                }
                            });

                            pinDialog.hide();
                        }
                    });
                    txvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pinDialog.hide();
                        }
                    });
                    pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    pinDialog.show();
                }
            });
        }
        linContainer.addView(cellLayout);
        final ScrollView scrollView = ((ScrollView)findViewById(R.id.scrollView));
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
    ////////// ********* Fire Base ********//////////
}
