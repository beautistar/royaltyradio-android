package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
