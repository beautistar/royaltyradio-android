package com.dj.royaltyradio.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devbrackets.android.playlistcore.listener.PlaylistListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.radioactivity.MainApplication;
import com.dj.royaltyradio.services.MediaItem;
import com.google.android.material.navigation.NavigationView;
import com.iamhabib.easy_preference.EasyPreference;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{


    DrawerLayout drawer;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ///Title ActionBar Transperent Custom
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        getWindow().getDecorView().setSystemUiVisibility(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ///// drawer Menu
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        // navigationView.inflateHeaderView(R.layout.draw_menu_header); ///programmatically
        navigationView.setNavigationItemSelectedListener(this);

        ImageView imvavartar = navigationView.getHeaderView(0).findViewById(R.id.avatar);
        TextView tvuser = navigationView.getHeaderView(0).findViewById(R.id.username);
        Glide.with(this).load(Commons.thisUser.getPicture()).placeholder(R.drawable.avatar).into((imvavartar));
        tvuser.setText(Commons.thisUser.getUsername());
        //drawer.openDrawer(GravityCompat.START, true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            displaySelectedScreen(menuItem.getItemId());
            return false;
    }
    private void displaySelectedScreen(int itemId) {
            Intent intent;
            switch (itemId) {
            case R.id.event:
                intent = new Intent(getApplicationContext(), EventActivity.class);
                startActivity(intent);
            break;
            case R.id.video:
                intent = new Intent(getApplicationContext(), VideoActivity.class);
                startActivity(intent);
            break;
            case R.id.radio:
                intent = new Intent(getApplicationContext(), RadioActivity.class);
                startActivity(intent);
            break;
            case R.id.social:
                intent = new Intent(getApplicationContext(), SocialActivity.class);
                startActivity(intent);
            break;
            case R.id.singout:
                logout();
            break;

            }
            //drawer.closeDrawer(GravityCompat.START);
        //finish();
    }

    public void onMyAccount(View view){
        Intent intent = new Intent(getApplicationContext(), MyAccountActivity.class);
        startActivity(intent);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void logout(){
        EasyPreference.with(getApplicationContext()).addString("easyEmail", "").save();
        Commons.thisUser = null;
        Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        MainApplication.getPlaylistManager().invokeStop();
        finish();
    }
}
