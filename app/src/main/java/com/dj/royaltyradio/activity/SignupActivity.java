package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.models.UserModel;
import com.iamhabib.easy_preference.EasyPreference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class SignupActivity extends BaseActivity {

    EditText editUsername;
    EditText editEmail;
    EditText editPassword;
    EditText editPasswordConfirm;
    ImageView imgAvatar;
    File file = null;

    AVLoadingIndicatorView progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        setupUI(findViewById(R.id.activity), this);
        progressBar = (AVLoadingIndicatorView)findViewById(R.id.loading_bar);
        imgAvatar = (ImageView) findViewById(R.id.img_avatar);
        editUsername = (EditText) findViewById(R.id.edit_username);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editPassword = (EditText) findViewById(R.id.edit_password);
        editPasswordConfirm = (EditText) findViewById(R.id.edit_password_confirm);

        ((Button)findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signup();
            }
        });
        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((FrameLayout)findViewById(R.id.frm_avatar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(SignupActivity.this);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri imgUri = result.getUri();
                file = new File(imgUri.getPath());
                imgAvatar.setImageURI(imgUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    void signup(){

        String username = editUsername.getText().toString().trim();
        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String passwordconfirm = editPasswordConfirm.getText().toString().trim();

        if(file == null){
            showToast("Take a photo");
            return;
        }

        if(username.length() == 0){
            editUsername.setError("Enter your name.");
            return;
        }
        if(email.length() == 0){
            editEmail.setError("Enter your email");
            return;
        }
        if(password.length() == 0){
            editPassword.setError("Enter your password");
            return;
        }
        if(passwordconfirm.length() == 0){
            editPasswordConfirm.setError("Enter your confirm password");
            return;
        }
        if (!password.equals(passwordconfirm)){
            showToast("Confirm your password");
        }
        if (!validateEmail(email)){
            editEmail.setError("Invalid email");
            return;
        }
        register(username, email, password);
    }

    private void register(String username, String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        /// API call
        AndroidNetworking.upload(Constants.SERVER + "register")
                .addMultipartFile("photo",file)
                .addMultipartParameter("username", username)
                .addMultipartParameter("email", email)
                .addMultipartParameter("password", password)
                .setTag(this)
                .setPriority(Priority.MEDIUM)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            String result = response.getString("result_code");
                            if(result.equals("0")){
                                showToast("Register success!");
                                finish();
                            }else if (result.equals("1")){
                                showToast("Email already exist");
                            }else if (result.equals("2")){
                                showToast("Upload file fail");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }
}
