package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;

public class SocialActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);

        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        // fb
        ((Button)findViewById(R.id.btn_fb)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Royaltyradio365/"));
                startActivity(intent);
            }
        });
        //// tw
        ((Button)findViewById(R.id.btn_twitter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/steviebmusic"));
                startActivity(intent);
            }
        });
        //// insta
        ((Button)findViewById(R.id.btn_instargram)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/royaltyradio365/"));
                startActivity(intent);
            }
        });
        //// homepage
        ((Button)findViewById(R.id.btn_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://steviebmusic.com"));
                startActivity(intent);
            }
        });

        // about us
        ((Button)findViewById(R.id.btn_about)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/steviebmusic/"));
                startActivity(intent);
            }
        });
    }
}
