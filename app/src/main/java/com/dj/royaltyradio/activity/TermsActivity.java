package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dj.royaltyradio.R;

import java.io.InputStream;

public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        String result;
        try {

            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.about);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);

        } catch (Exception e) {

            // e.printStackTrace();
            result = "Sorry, Now editing..";

        }

        ((TextView)findViewById(R.id.tv_terms)).setText(result);
    }
}
