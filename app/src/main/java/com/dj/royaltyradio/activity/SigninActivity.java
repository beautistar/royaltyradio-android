package com.dj.royaltyradio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.models.UserModel;
import com.dj.royaltyradio.radioactivity.framgements.Home;
import com.iamhabib.easy_preference.EasyPreference;
import com.wang.avi.AVLoadingIndicatorView;
import org.json.JSONException;
import org.json.JSONObject;

public class SigninActivity extends BaseActivity {

    EditText editEmail;
    EditText editPassword;

    AVLoadingIndicatorView progressBar;
    CheckBox checkBox;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        setupUI(findViewById(R.id.activity), this);
        progressBar = (AVLoadingIndicatorView)findViewById(R.id.loading_bar);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editPassword = (EditText) findViewById(R.id.edit_password);

        ((Button)findViewById(R.id.btn_signin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signin();
            }
        });

        ((Button)findViewById(R.id.btn_signup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
            }
        });
        ((TextView)findViewById(R.id.tv_forgot)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ForgotActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ((Button)findViewById(R.id.btn_guest)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Commons.bGuest = true;
                Intent intent = new Intent(getApplicationContext(), RadioActivity.class);
                startActivity(intent);
            }
        });

        //checkBox = (CheckBox) findViewById(R.id.chk_terms);

    }



    void signin(){

        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();

        if(email.length() == 0){
            editEmail.setError("Enter your email.");
            return;
        }
        if(password.length() == 0){
            editPassword.setError("Enter your password.");
            return;
        }
        if (!validateEmail(email)){
            editEmail.setError("Invalid email");
            return;
        }

        /*if ( !checkBox.isChecked() )
        {
            checkBox.setBackgroundResource(R.drawable.edit_red_line);
            *//*Intent intent = new Intent(SigninActivity.this, TermsActivity.class);
            startActivity(intent);*//*
            return;
        } else
        {
            *//*checkBox.setBackgroundResource(R.drawable.edit_default_line);*//*
        }*/

        login(email, password);
    }

    private void login(String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        /// API call
        AndroidNetworking.post(Constants.SERVER + "login")
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            String result = response.getString("result_code");
                            if(result.equals("0")){
                                JSONObject userObj = response.getJSONObject("user_info");
                                UserModel userModel = new UserModel();
                                userModel.setId(userObj.getInt("user_id"));
                                userModel.setUsername(userObj.getString("username"));
                                userModel.setEmail(userObj.getString("email"));
                                userModel.setPicture(userObj.getString("photo"));
                                userModel.setReport(userObj.getString("report"));
                                userModel.setBlock(userObj.getString("block"));
                                userModel.setIs_admin(userObj.getInt("is_admin"));
                                Commons.thisUser = userModel;
                                EasyPreference.with(SigninActivity.this).addString("easyEmail", Commons.thisUser.getEmail()).save();
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }else if (result.equals("1")){
                                showToast("Non exist email");
                            }
                            else if (result.equals("2")){
                                showToast("Password incorrect");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }
}
