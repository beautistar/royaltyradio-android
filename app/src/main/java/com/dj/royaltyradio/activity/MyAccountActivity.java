package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.bumptech.glide.Glide;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class MyAccountActivity extends BaseActivity {

    EditText editUsername;
    TextView editEmail;
    EditText editPassword;
    EditText editPasswordConfirm;
    ImageView imgAvatar;
    File file = null;

    AVLoadingIndicatorView progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        setupUI(findViewById(R.id.activity), this);

        progressBar = (AVLoadingIndicatorView)findViewById(R.id.loading_bar);
        imgAvatar = (ImageView) findViewById(R.id.img_avatar);
        editUsername = (EditText) findViewById(R.id.edit_username);
        editEmail = (TextView) findViewById(R.id.edit_email);
        editPassword = (EditText) findViewById(R.id.edit_password);
        editPasswordConfirm = (EditText) findViewById(R.id.edit_password_confirm);

        ((Button)findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onChange();
            }
        });
        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((FrameLayout)findViewById(R.id.frm_avatar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(MyAccountActivity.this);
            }
        });
        init();
    }

    void init(){
        /////////// setting userInfo
        Glide.with(this).load(Commons.thisUser.getPicture()).placeholder(R.drawable.user).into((imgAvatar));
        file = new File(Commons.thisUser.getPicture());

        editUsername.setText(Commons.thisUser.getUsername());
        editEmail.setText(Commons.thisUser.getEmail());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri imgUri = result.getUri();
                file = new File(imgUri.getPath());
                //showToast(file.getPath());
                imgAvatar.setImageURI(imgUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    void onChange(){

        String username = editUsername.getText().toString().trim();
        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String passwordconfirm = editPasswordConfirm.getText().toString().trim();

        //////  creation a custom file when file is non exist
        if(file == null){
            showToast("Take a photo");
            return;
        }

        if(username.length() == 0){
            editUsername.setError("Enter your name.");
            return;
        }
        if(email.length() == 0){
            editEmail.setError("Enter your email");
            return;
        }
        if(password.length() == 0){
            editPassword.setError("Enter your password");
            return;
        }
        if(passwordconfirm.length() == 0){
            editPasswordConfirm.setError("Enter your confirm password");
            return;
        }
        if (!password.equals(passwordconfirm)){
            showToast("Confirm your password");
        }
        if (!validateEmail(email)){
            editEmail.setError("Invalid email");
            return;
        }
        changeAccount(username, email, password);
    }

    private void changeAccount(final String username, final String email, final String password) {
        progressBar.setVisibility(View.VISIBLE);
        /// API call

        if (file.getPath().contains("http")) {
            AndroidNetworking.upload(Constants.SERVER + "changeAccount")
                    .addMultipartParameter("username", username)
                    .addMultipartParameter("email", email)
                    .addMultipartParameter("password", password)
                    .setTag(this)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                            progressBar.setVisibility(View.VISIBLE);
                        }
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressBar.setVisibility(View.GONE);
                            try {
                                String result = response.getString("result_code");
                                if (result.equals("0")) {
                                    showToast("Register success!");
                                    if (!file.getPath().contains("http")){
                                        Commons.thisUser.setPicture(response.getString("photo"));
                                    }
                                    Commons.thisUser.setPassword(password);
                                    Commons.thisUser.setUsername(username);
                                    //showToast(username + "\n" + email + "\n" + password + "\n" + Commons.thisUser.getPicture());
                                    finish();
                                } else if (result.equals("1")) {
                                    showToast("Upload file fail");
                                } else if (result.equals("2")) {
                                    showToast("Upload file fail");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } else {

            AndroidNetworking.upload(Constants.SERVER + "changeAccount")
                    .addMultipartFile("photo", file)
                    .addMultipartParameter("username", username)
                    .addMultipartParameter("email", email)
                    .addMultipartParameter("password", password)
                    .setTag(this)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                            progressBar.setVisibility(View.VISIBLE);
                        }
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressBar.setVisibility(View.GONE);
                            try {
                                String result = response.getString("result_code");
                                if (result.equals("0")) {
                                    showToast("Update success!");
                                        Commons.thisUser.setPicture(response.getString("photo"));
                                    Commons.thisUser.setPassword(password);
                                    Commons.thisUser.setUsername(username);
                                    showToast(username + "\n" + email + "\n" + password + "\n" + Commons.thisUser.getPicture());
                                    finish();
                                } else if (result.equals("1")) {
                                    showToast("Upload file fail");
                                } else if (result.equals("2")) {
                                    showToast("Upload file fail");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }


    }
}