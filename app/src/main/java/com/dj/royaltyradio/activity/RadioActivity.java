package com.dj.royaltyradio.activity;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.devbrackets.android.playlistcore.data.PlaybackState;
import com.devbrackets.android.playlistcore.listener.PlaylistListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Commons;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.radioactivity.MainApplication;
import com.dj.royaltyradio.services.IcyStreamMeta;
import com.dj.royaltyradio.services.MediaItem;
import com.dj.royaltyradio.services.PlaylistManager;
import com.dj.royaltyradio.services.Samples;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.material.navigation.NavigationView;
import com.iamhabib.easy_preference.EasyPreference;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.RequiresApi;
import androidx.drawerlayout.widget.DrawerLayout;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RadioActivity extends BaseActivity  implements PlaylistListener<MediaItem>{
    /*NavigationView.OnNavigationItemSelectedListener,  */
    //// UI Variable
    ImageView playButton, pauseButton;
    TextView tvTitle, tvArtist;
    boolean bPlaying;
    //Intent streamService;
    LinearLayout linContainer;
    //// radio Variable
    IcyStreamMeta streamMeta;
    MetadataTask metadataTask; /// Meta Data Class
    private Handler mHandler;
    private Timer timer = new Timer();
    private MyTimerTask task = new MyTimerTask(); // sound related
    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private PlaylistManager playlistManager;

    ///// Firebase Variable
    Firebase refAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);
        //setupUI(findViewById(R.id.activity), this);

        /// initial when Guest way.
        if (Commons.bGuest){
            ((ImageView)findViewById(R.id.img_back)).setVisibility(View.GONE);
        }
        playButton = (ImageView) findViewById(R.id.imv_play);
        pauseButton = (ImageView) findViewById(R.id.imv_pause);
        tvArtist = (TextView)findViewById(R.id.tv_artist);
        tvTitle = (TextView)findViewById(R.id.tv_title);

        ((ImageView)findViewById(R.id.img_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //drawer.openDrawer(GravityCompat.START, true);
                finish();
            }
        });
        //----------------- UI version test start ----------------------////
        /**********FireBase *************/

        Firebase.setAndroidContext(this);
        //FirebaseApp.initializeApp(this);
        refAudio = new Firebase(Constants.FIREBASE_URL + "message/" + Constants.FIREBASE_ID_AUDIO);

        linContainer = (LinearLayout) findViewById(R.id.lin_container);

        ((ImageView)findViewById(R.id.imv_send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Commons.bGuest){
                    showToast("You need to login first");
                    return;
                }
                if (Commons.thisUser.getReport().equals("report")){
                    showToast("Sorry, you were reported.");
                    return;
                }
                if (Commons.thisUser.getBlock().equals("block")){
                    showToast("Sorry, you were blocked.");
                    return;
                }
                String message = ((EditText)findViewById(R.id.edit_write)).getText().toString().trim();
                ((EditText)findViewById(R.id.edit_write)).setText("");
                if (message.equals(""))
                    return;


                Map<String, String> map = new HashMap<String, String>();
                map.put("message", message);
                map.put("time", String.valueOf(new Date().getTime()));
                map.put("sender_id", String.valueOf(Commons.thisUser.getId()));
                map.put("name", Commons.thisUser.getUsername());
                //map.put("email", Commons.thisUser.getEmail());
                map.put("photo", Commons.thisUser.getPicture());
                refAudio.push().setValue(map);
                Log.d("send====>", map.toString());
            }
        });

        refAudio.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String senderId = map.get("sender_id").toString();
                String name = map.get("name").toString();
                //String email = map.get("email").toString();
                String photo = map.get("photo").toString();
                String time = map.get("time").toString();
                String key = dataSnapshot.getKey();

                if(senderId.equals(String.valueOf(Commons.thisUser.getId()))){
                    addMessageBox(senderId, message, time, photo, name, 1);
                } else {
                    addMessageBox(senderId, message, time, photo, name, 0);
                }
                Log.d("recevie====>", key);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        mHandler = new Handler();


        /**********FireBase *************/
        //----------------- UI version test end ----------------------////

        /*bPlaying = EasyPreference.with(RadioActivity.this).getBoolean("isPlaying", false);
        if (bPlaying){
            playButton.setVisibility(View.GONE);
            pauseButton.setVisibility(View.VISIBLE);
        } else {
            playButton.setVisibility(View.VISIBLE);
            pauseButton.setVisibility(View.GONE);
        }*/

        bPlaying = EasyPreference.with(RadioActivity.this).getBoolean("isPlaying", false);
        if (MainApplication.getPlaylistManager().getMediaPlayers().isEmpty()){
            EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", false);
            bPlaying = false;
        }

        if (bPlaying){
            playButton.setVisibility(View.GONE);
            pauseButton.setVisibility(View.VISIBLE);
            updateStream();
        } else {
            playButton.setVisibility(View.VISIBLE);
            pauseButton.setVisibility(View.GONE);
        }
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /*playButton.setVisibility(View.GONE);
                pauseButton.setVisibility(View.VISIBLE);
                EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", true).save();
*/
                playlistManager.setCurrentPosition(0);
                playlistManager.play(0, false);

                updateStream();
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /*playButton.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.GONE);
                EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", false).save();
*/
                playlistManager.invokePausePlay();
                timer.cancel();
                mHandler.removeCallbacks(mStatusChecker);
            }
        });

        setupPlaylistManager();

    }

    ////////// ********* Fire Base ********//////////
    public void addMessageBox(final String user_id, final String message, final String time, final String photoUrl, String name, int type){

        final LinearLayout cellLayout;
        if (type == 1)
        {
            cellLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_chat_me, null);
        } else {
            cellLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_chat, null);
        }
        Glide.with(getApplicationContext()).load(photoUrl).placeholder(R.drawable.avatar).into(((ImageView)cellLayout.findViewById(R.id.imv_user)));
        ((ImageView)cellLayout.findViewById(R.id.imv_user)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ViewImageActivity.class);
                intent.putExtra("image", photoUrl);
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(RadioActivity.this, (ImageView)cellLayout.findViewById(R.id.imv_user), getString(R.string.transition));
                startActivity(intent, transitionActivityOptions.toBundle());
            }
        });
        ((TextView)cellLayout.findViewById(R.id.tv_username)).setText(name);
        ((TextView)cellLayout.findViewById(R.id.tv_message)).setText(message);
        ((TextView)cellLayout.findViewById(R.id.tv_date)).setText(getCurrentTimeString(Long.parseLong(time)));

        if (Commons.thisUser.getIs_admin() == 1){
            cellLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View pinDialogView = LayoutInflater.from(RadioActivity.this).inflate(R.layout.alert_user_settings, null, false);
                    final androidx.appcompat.app.AlertDialog pinDialog = new androidx.appcompat.app.AlertDialog.Builder(RadioActivity.this).create();
                    pinDialog.setView(pinDialogView);

                    WindowManager.LayoutParams wmlp = pinDialog.getWindow().getAttributes();
                    wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                    /*wmlp.x = Gravity.CENTER - wmlp.width/2 - 20;   //x position*/
                    wmlp.y = 40;   //y position

                    TextView txvReport = (TextView) pinDialogView.findViewById(R.id.tv_report);
                    TextView txvBlock = (TextView) pinDialogView.findViewById(R.id.tv_block);
                    TextView txvDelete = (TextView) pinDialogView.findViewById(R.id.tv_delete);
                    TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
                    txvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pinDialog.hide();
                        }
                    });
                    txvReport.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            /// API call
                            AndroidNetworking.post(Constants.SERVER + "report")
                                    .addBodyParameter("user_id", user_id)
                                    .setTag(this)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                String result = response.getString("result_code");
                                                if (result.equals("0")) {
                                                    showToast("This user is reported.");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            // handle error
                                        }
                                    });

                            pinDialog.hide();
                        }
                    });
                    txvBlock.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            /// API call
                            AndroidNetworking.post(Constants.SERVER + "block")
                                    .addBodyParameter("user_id", user_id)
                                    .setTag(this)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                String result = response.getString("result_code");
                                                if (result.equals("0")) {
                                                    showToast("This user is blocked.");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            // handle error
                                        }
                                    });

                            pinDialog.hide();
                        }
                    });
                    txvDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            linContainer.removeView(cellLayout);

                            refAudio.orderByChild("time").equalTo(time).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        HashMap<String, Object> dataMap = (HashMap<String, Object>) dataSnapshot.getValue();
                                        for (String key : dataMap.keySet()){
                                            Object data = dataMap.get(key);
                                            try{
                                                HashMap<String, Object> requestData = (HashMap<String, Object>) data;

                                                    dataSnapshot.getRef().child(key).removeValue();

                                            }catch (ClassCastException cce){
                                                // If the object can’t be casted into HashMap, it means that it is of type String.
                                                try{
                                                    String mString = String.valueOf(dataMap.get(key));
                                                    Log.d("datasnaptKey==>", mString);
                                                }catch (ClassCastException cce2){
                                                }
                                            }
                                        }
                                    }

                                }
                                @Override
                                public void onCancelled(FirebaseError firebaseError) {
                                    Log.d("User",firebaseError.getMessage() );
                                }
                            });

                            pinDialog.hide();
                        }
                    });
                    txvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pinDialog.hide();
                        }
                    });
                    pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    pinDialog.show();
                }
            });
        }
        linContainer.addView(cellLayout);
        final ScrollView scrollView = ((ScrollView)findViewById(R.id.scrollView));
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        /*cellLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View pinDialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alert_user_settings, null, false);
                final androidx.appcompat.app.AlertDialog pinDialog = new androidx.appcompat.app.AlertDialog.Builder(getApplicationContext()).create();
                pinDialog.setView(pinDialogView);
                TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
                TextView txvReport = (TextView) pinDialogView.findViewById(R.id.tv_report);
                TextView txvBlock = (TextView) pinDialogView.findViewById(R.id.tv_block);
                TextView txvDelete = (TextView) pinDialogView.findViewById(R.id.tv_delete);
                txvCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pinDialog.hide();
                    }
                });
                txvBlock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pinDialog.hide();
                        EasyPreference.with(getApplicationContext()).addObject("blockUsers", Commons.blockUsers.add(email)).save();
                    }
                });
                txvReport.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pinDialog.hide();
                    }
                });
                txvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pinDialog.hide();
                    }
                });
                pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                pinDialog.show();
            }
        });*/
    }
    ////////// ********* Fire Base ********//////////

    private void setupPlaylistManager() {
        Log.d("PLISTMANAGER=====>", "START");
        playlistManager = MainApplication.getPlaylistManager();

        if (playlistManager.getId() == 1) {
            Log.d("PLISTMANAGER===1==>", "return");
            return;
        }

        List<MediaItem> mediaItems = new LinkedList<>();
        for (Samples.Sample sample : Samples.getAudioSamples()) {
            MediaItem mediaItem = new MediaItem(sample, true);
            mediaItems.add(mediaItem);
        }
        Log.d("PLISTMANAGER===2==>", "mediaitem");

        playlistManager.setParameters(mediaItems, 0);
        playlistManager.setId(1);
        Log.d("PLISTMANAGER===3==>", "finish");

    }

    @Override
    protected void onResume() {
        super.onResume();

        playlistManager = MainApplication.getPlaylistManager();
        playlistManager.registerPlaylistListener(this);
        Log.d("registerPLISTMANAGER", "done");

        if (Commons.bGuest){
            //Commons.bGuest = false;
            return;
        }
        boolean bVideo = EasyPreference.with(RadioActivity.this).getBoolean("isPlayingVideo", false);
        if (bVideo){
            Log.d("#####!!!!!!!####", "VideoActivity");
            timer = new Timer();
            task = new MyTimerTask();
            timer.schedule(task,1000, mInterval);
            mStatusChecker.run();
            playlistManager.play(0, false);
            EasyPreference.with(RadioActivity.this).addBoolean("isPlayingVideo", false).save();
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        playlistManager.unRegisterPlaylistListener(this);
        mHandler.removeCallbacks(mStatusChecker);
        timer.cancel();
    }


    @Override
    public boolean onPlaybackStateChanged(@NotNull PlaybackState playbackState) {
        bPlaying = EasyPreference.with(RadioActivity.this).getBoolean("isPlaying", false);

        Log.d("playbackstate==>", String.valueOf(playbackState));
        switch (playbackState) {
            case STOPPED:
                //if (bPlaying) {
                    bPlaying = false;
                    EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", false).save();
                    playButton.setVisibility(View.VISIBLE);
                    pauseButton.setVisibility(View.GONE);
                //}
                break;
            case PLAYING:
                //if (!bPlaying) {
                    bPlaying = true;
                    EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", true).save();
                    playButton.setVisibility(View.GONE);
                    pauseButton.setVisibility(View.VISIBLE);
                //}
                break;
            case PAUSED:
                //if (bPlaying) {
                    bPlaying = false;
                    EasyPreference.with(RadioActivity.this).addBoolean("isPlaying", false).save();
                    playButton.setVisibility(View.VISIBLE);
                    pauseButton.setVisibility(View.GONE);
                //}
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onPlaylistItemChanged(@Nullable MediaItem currentItem, boolean b, boolean b1) {
        return false;
    }
    private void updateTitle() {
        tvArtist.setText(MainApplication.get_song_artist());
        tvTitle.setText(MainApplication.get_song_title());
    }

    private void updateStream() {
        streamMeta = new IcyStreamMeta();
        try {
            streamMeta.setStreamUrl(new URL(Constants.RADIOURL));
            Log.d("*************",streamMeta.getStreamUrl().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        metadataTask =new MetadataTask();
        try {
            metadataTask.execute(new URL(Constants.RADIOURL));
            Log.d("RADIO_EXCUTE", "START");

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("RADIO_EXCUTE", e.toString());
        }
        MainApplication.setIcyStreamMeta(streamMeta);

        timer = new Timer();
        task = new MyTimerTask();
        timer.schedule(task,1000, mInterval);
        mStatusChecker.run();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateTitle(); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Commons.bGuest = false;

        Log.e("Exception", " broadcast receiver ");
        //EasyPreference.with(getApplicationContext()).addBoolean("isPlaying", false).save();
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("sreamMeta===>", e.toString());
            }
            try {
                String song_artist=streamMeta.getStreamTitle();
                Log.i("ARTIST TITLE", song_artist);

                if (song_artist.contains("-")){
                    String song_data[] = song_artist.split("-");
                    if (song_data[0] != null){
                        MainApplication.set_song_artist(song_data[0].trim());
                    }
                    if (song_data[1] != null){
                        MainApplication.set_song_title(song_data[1].trim());
                    }
                } else if (song_artist.contains(":")){
                    String song_data[] = song_artist.split(":");
                    if (song_data[0] != null){
                        MainApplication.set_song_artist(song_data[0].trim());
                    }
                    if (song_data[1] != null){
                        MainApplication.set_song_title(song_data[1].trim());
                    }
                } else {
                    MainApplication.set_song_artist(song_artist);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("sreamMeta==exception=>", e.toString());
            }
        }
    }
    protected class MetadataTask extends AsyncTask<URL, Void, IcyStreamMeta>
    {
        @Override
        protected IcyStreamMeta doInBackground(URL... urls)
        {

            try
            {
                streamMeta.refreshMeta();
            }
            catch (IOException e)
            {
                Log.e(MetadataTask.class.toString(), e.getMessage());
            }
            return streamMeta;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(IcyStreamMeta result)
        {
            try
            {
                String song_artist = streamMeta.getStreamTitle();
                if (song_artist.contains("-")){
                    String song_data[] = song_artist.split("-");
                    if (song_data[0] != null){
                        MainApplication.set_song_artist(song_data[0].trim());
                    }
                    if (song_data[1] != null){
                        MainApplication.set_song_title(song_data[1].trim());
                    }
                }
                if(MainApplication.get_song_artist().length()>0)
                {
                    updateTitle();
                }
            }
            catch (IOException e)
            {
                Log.e(MetadataTask.class.toString(), e.getMessage());
            }
        }
    }

}
