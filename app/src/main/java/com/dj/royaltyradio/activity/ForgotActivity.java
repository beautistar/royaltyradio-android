package com.dj.royaltyradio.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.commons.Constants;
import com.dj.royaltyradio.models.UserModel;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotActivity extends BaseActivity {


    EditText editEmail;
    EditText editPin;
    String pincode;

    AVLoadingIndicatorView progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        setupUI(findViewById(R.id.activity), this);
        progressBar = (AVLoadingIndicatorView)findViewById(R.id.loading_bar);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editPin = (EditText) findViewById(R.id.edit_pin);

        ((Button)findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgot();
            }
        });
        ((Button)findViewById(R.id.btn_pin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPin();
            }
        });
        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    void forgot(){

        String email = editEmail.getText().toString().trim();
        if(email.length() == 0){
            editEmail.setError("Enter your email");
            return;
        }
        if (!validateEmail(email)){
            editEmail.setError("Invalid email");
            return;
        }
        onForgot(email);
    }
    void onPin()
    {
        String pin = editPin.getText().toString().trim();
        if(pin.length() == 0){
            showToast("Enter your pin code");
            return;
        }
        if (!pin.equals(pincode)){
            showToast("Invalid pin code");
            return;
        }
        Intent intent = new Intent(getApplicationContext(), RadioActivity.class);
        startActivity(intent);
        finish();
    }

    private void onForgot(String email){
        progressBar.setVisibility(View.VISIBLE);
        /// API call
        AndroidNetworking.post(Constants.SERVER + "forgot")
                .addBodyParameter("email", email)
                .setTag(this)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        // do anything with response
                        try {
                            String result = response.getString("result_code");
                            if(result.equals("0")){
                                pincode = response.getString("pincode");
                                ((LinearLayout)findViewById(R.id.lin_pin)).setVisibility(View.VISIBLE);
                            }else if (result.equals("1")){
                                showToast("Non exist email");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }
}
