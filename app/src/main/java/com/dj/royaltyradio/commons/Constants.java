package com.dj.royaltyradio.commons;

public class Constants {
    public static final String SERVER = "http://www.royaltyradio.us/api/";
    public static final String RADIOURL = "http://usa10.fastcast4u.com:1470/stream";
    public static final String FIREBASE_URL = "https://royalty-radio.firebaseio.com/";
    public static final String FIREBASE_ID_AUDIO = "royaltyradio_audio";
    public static final String FIREBASE_ID_VIDEO = "royaltyradio_video";

}
