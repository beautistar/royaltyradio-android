package com.dj.royaltyradio.commons;

import com.dj.royaltyradio.models.EventModel;
import com.dj.royaltyradio.models.UserModel;
import com.dj.royaltyradio.models.VideoModel;

import java.util.ArrayList;

public class Commons {

    public static UserModel thisUser = new UserModel();
    public static boolean bGuest = false;
    public static ArrayList<String> blockUsers = new ArrayList<>();
    public static boolean bImageViewState = false;

}
