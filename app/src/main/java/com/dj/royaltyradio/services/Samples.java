package com.dj.royaltyradio.services;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Samples {
    @NonNull
    private static final List<Sample> audioSamples;
    @NonNull
    private static final List<Sample> videoSamples;

    static {
        String audioImage = "";

        /* AUDIO
         * These items are licensed under CreativeCommons from LibriVox
         * Additional files can be found at https://archive.org/details/count_monte_cristo_0711_librivox
         */
        audioSamples = new LinkedList<>();
        audioSamples.add(new Sample(0, "Royalty Radio", "http://usa10.fastcast4u.com:1470/stream", "", "", ""));

        /* VIDEO
         * These items are licensed under multiple Public Domain and Open licenses
         */
        videoSamples = new ArrayList<>();
        videoSamples.add(new Sample(0, "Big Buck Bunny", "http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_10mb.mp4"));
        videoSamples.add(new Sample(1, "Sintel", "https://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"));
        videoSamples.add(new Sample(2, "Popeye for President", "https://archive.org/download/Popeye_forPresident/Popeye_forPresident_512kb.mp4"));
        videoSamples.add(new Sample(3, "Caminandes: Llama Drama", "http://amssamples.streaming.mediaservices.windows.net/634cd01c-6822-4630-8444-8dd6279f94c6/CaminandesLlamaDrama4K.ism/manifest"));
    }
    @NonNull
    public static List<Sample> getAudioSamples() {
        return audioSamples;
    }

    @NonNull
    public static List<Sample> getVideoSamples() {
        return videoSamples;
    }
     /**
     * A container for the information associated with a
     * sample media item.
     */
    public static class Sample {
        @NonNull
        private int id;
        @NonNull
        private String title;
        @NonNull
        private String mediaUrl;
        @Nullable
        private String artworkUrl;
        @Nullable
        private String album;
        @Nullable
        private String artist;

        public Sample(@NonNull int id, @NonNull String title, @NonNull String mediaUrl) {
            this(id, title, mediaUrl, null, null, null);
        }

        public Sample(@NonNull int id, @NonNull String title, @NonNull String mediaUrl, @Nullable String artworkUrl, @Nullable String album, @Nullable String artist) {
            this.id = id;
            this.title = title;
            this.mediaUrl = mediaUrl;
            this.artworkUrl = artworkUrl;
            this.album = album;
            this.artist = artist;
        }

        @NonNull
        public int getId() {
            return id;
        }

        @NonNull
        public String getTitle() {
            return title;
        }

        @NonNull
        public String getMediaUrl() {
            return mediaUrl;
        }

        @Nullable
        public String getArtworkUrl() {
            return artworkUrl;
        }

        @Nullable
        public String getAlbum() {
            return album;
        }

        @Nullable
        public String getArtist() {
            return artist;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
