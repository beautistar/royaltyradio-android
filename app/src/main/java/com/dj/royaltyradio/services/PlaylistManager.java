package com.dj.royaltyradio.services;

import android.app.Application;

import com.devbrackets.android.playlistcore.manager.ListPlaylistManager;


/**
 * A PlaylistManager that extends the {@link ListPlaylistManager} for use with the
 * {@link MediaService} which extends {@link com.devbrackets.android.playlistcore.service.BasePlaylistService}.
 */
public class PlaylistManager extends ListPlaylistManager<MediaItem> {

    public PlaylistManager(Application application) {
        super(application, MediaService.class);
    }

}
