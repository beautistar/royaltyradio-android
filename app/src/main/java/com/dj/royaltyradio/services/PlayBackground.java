package com.dj.royaltyradio.services;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.exoplayer.ExoPlayer;

/**
 * Created by dragon on 11/24/2016.
 */

public class PlayBackground extends AsyncTask<String, String, String>{

    private ExoPlayer exoPlayer;
    private boolean play;
    /**
     * Before starting background thread Show Progress Bar Dialog
     * */
    public PlayBackground(ExoPlayer exoPlayer, boolean play){
        this.exoPlayer = exoPlayer;
        this.play = play;
    }

    public ExoPlayer getExoPlayer(){
        return this.exoPlayer;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        try {

            exoPlayer.setPlayWhenReady(play);

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        exoPlayer.setPlayWhenReady(false);
    }
}
