package com.dj.royaltyradio.radioactivity.utilitie;

import android.content.Context;
import android.graphics.Typeface;

public class FontsManager {
    String OPENSANS_LIGHT = "opensans-light.ttf";
    String OPENSANS_REGULAR = "opensans-regular.ttf";
    String OPENSANS_SEMIBOLD = "opensans-semibold.ttf";
    public static Typeface getTypeface(String font, Context context){

        return Typeface.createFromAsset(context.getAssets(),"fonts/font");
    }
}