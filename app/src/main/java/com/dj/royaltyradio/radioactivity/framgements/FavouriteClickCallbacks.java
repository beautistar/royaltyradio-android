package com.dj.royaltyradio.radioactivity.framgements;


import com.dj.royaltyradio.radioactivity.models.Station;
import com.dj.royaltyradio.radioactivity.models.StationAddedManually;

/**
 * Created by ronak on 01/23/17.
 */
public interface FavouriteClickCallbacks {
    public void favouriteAdded(Station station, int position);

    public void favouriteRemoved(Station station, int position);

    public void favrouriteDeleted(StationAddedManually manually, int adapterPosition);
}
