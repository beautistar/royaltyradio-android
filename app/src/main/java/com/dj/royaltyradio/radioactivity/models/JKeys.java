package com.dj.royaltyradio.radioactivity.models;

public class JKeys {

    public static final String TUNE_IN = "tunein";
    public static final String BASE = "base";
    public static final String BASE_M3U = "base-m3u";
    public static final String BASE_XSPF = "base-xspf";
    public static final String STATION_LIST = "stationlist";
    public static final String STATION = "station";
    public static final String NAME = "name";
    public static final String LOGO = "logo";
    public static final String ID = "id";
    public static final String BR = "br";
    public static final String CT = "ct";
    public static final String GENRE = "genre";
    public static final String LC = "lc";
    public static final String MT = "mt";
    public static final String ML = "ml";
    public static final String CST = "cst";
    public static final String GENRE2 = "genre2";
    public static final String GENRE3 = "genre3";
    public static final String URI = "uri";
    public static final String DESCRIPTION = "description";
    public static final java.lang.String STATION_ID = "station_id";

}
