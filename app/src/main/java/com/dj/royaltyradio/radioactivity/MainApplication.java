package com.dj.royaltyradio.radioactivity;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Log;

import com.dj.royaltyradio.R;
import com.dj.royaltyradio.models.RadioModel;
import com.dj.royaltyradio.services.IcyStreamMeta;
import com.dj.royaltyradio.services.PlayBackground;
import com.dj.royaltyradio.services.PlaylistManager;
import com.firebase.client.Firebase;
import com.google.android.exoplayer.ExoPlayer;
import com.google.firebase.FirebaseApp;
import com.orm.SugarContext;

import java.util.ArrayList;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ronak on 01/23/17
 */

public class MainApplication extends MultiDexApplication {

    private static MainApplication _application;
    private RadioModel radioModel = new RadioModel();
    private static ExoPlayer _exoPlayer;
    private static PlayBackground _player;
    private static String _song_artist = "";
    private static String _song_title = "";
    private static IcyStreamMeta _icyStreamMeta;
    private static PlaylistManager _playlistManager;
    private static int _currentRadio = 0;

    private static final String LOG_TAG = "CrashCatch";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());
//        TypefaceUtil.overrideFont(getApplicationContext(), "serif", "fonts/font-light.ttf");

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/dosis-regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        _application = this;
        _playlistManager = new PlaylistManager(this);
        _exoPlayer = ExoPlayer.Factory.newInstance(1);
        _player = new PlayBackground(_exoPlayer, false);
        Log.d("APPLICATION=====>", "INITIAL");


        Firebase.setAndroidContext(this);
        FirebaseApp.initializeApp(this);

        startCatcher();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        _application = null;
        _playlistManager = null;
    }

    public static void setExoPlayer(ExoPlayer exoPlayer) {
        _exoPlayer = exoPlayer;
    }

    public static ExoPlayer getExoPlayer() {
        return _exoPlayer;
    }

    public static void setPlayer(PlayBackground playBackground) {
        _player = playBackground;
    }

    public static PlayBackground getPlayer() {
        return _player;
    }

    public static PlaylistManager getPlaylistManager() {
        return _playlistManager;
    }

    public static String get_song_artist() {
        return _song_artist;
    }

    public static void set_song_artist(String _song_artist) {
        MainApplication._song_artist = _song_artist;
    }

    public static String get_song_title() {
        return _song_title;
    }

    public static void set_song_title(String _song_title) {
        MainApplication._song_title = _song_title;
    }

    public static IcyStreamMeta getIcyStreamMeta() {
        return _icyStreamMeta;
    }

    public static void setIcyStreamMeta(IcyStreamMeta icyStreamMeta) {
        _icyStreamMeta = icyStreamMeta;
    }


    public static synchronized MainApplication getInstance() {

        return _application;
    }

    public static Context getAppContext() {
        return _application.getApplicationContext();
    }

    // Crash

    private void startCatcher() {
        Thread.UncaughtExceptionHandler systemUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
        // the following handler is used to catch exceptions thrown in background threads
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtHandler(new Handler()));

        while (true) {
            try {
                Log.v(LOG_TAG, "Starting crash catch Looper");
                Looper.loop();
                Thread.setDefaultUncaughtExceptionHandler(systemUncaughtHandler);
                throw new RuntimeException("Main thread loop unexpectedly exited");
            } catch (BackgroundException e) {
                Log.e(LOG_TAG, "Caught the exception in the background thread " + e.threadName + ", TID: " + e.tid, e.getCause());
                showCrashDisplayActivity(e.getCause());
            } catch (Throwable e) {
                Log.e(LOG_TAG, "Caught the exception in the UI thread, e:", e);
                showCrashDisplayActivity(e);
            }
        }
    }

    void showCrashDisplayActivity(Throwable e) {
//        Intent i = new Intent(this, CrashDisplayActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.putExtra("e", e);
//        startActivity(i);
    }


    /**
     * This handler catches exceptions in the background threads and propagates them to the UI thread
     */
    static class UncaughtHandler implements Thread.UncaughtExceptionHandler {

        private final Handler mHandler;

        UncaughtHandler(Handler handler) {
            mHandler = handler;
        }

        public void uncaughtException(Thread thread, final Throwable e) {
            Log.v(LOG_TAG, "Caught the exception in the background " + thread + " propagating it to the UI thread, e:", e);
            final int tid = Process.myTid();
            final String threadName = thread.getName();
            mHandler.post(new Runnable() {
                public void run() {
                    throw new BackgroundException(e, tid, threadName);
                }
            });
        }
    }

    /**
     * Wrapper class for exceptions caught in the background
     */
    static class BackgroundException extends RuntimeException {

        final int tid;
        final String threadName;

        /**
         * @param e original exception
         * @param tid id of the thread where exception occurred
         * @param threadName name of the thread where exception occurred
         */
        BackgroundException(Throwable e, int tid, String threadName) {
            super(e);
            this.tid = tid;
            this.threadName = threadName;
        }
    }
}
