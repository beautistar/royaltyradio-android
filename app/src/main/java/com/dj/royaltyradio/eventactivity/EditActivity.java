package com.dj.royaltyradio.eventactivity;

import android.Manifest;
import android.content.AsyncQueryHandler;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dj.royaltyradio.R;
import com.dj.royaltyradio.base.BaseActivity;
import com.dj.royaltyradio.eventactivity.content.CalendarCursor;
import com.dj.royaltyradio.eventactivity.content.EventCursor;
import com.dj.royaltyradio.eventactivity.widget.EventEditView;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

public class EditActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * {@link android.os.Parcelable} extra contains {@link EventEditView.Event} to edit
     */
    public static final String EXTRA_EVENT = "extra:event";
    private static final String STATE_EVENT = "state:event";
    private static final String EXTRA_CALENDAR_ID = "extra:calendarId";
    private static final int LOADER_CALENDARS = 0;
    private static final int LOADER_SELECTED_CALENDAR = 1;

    private EventEditView mEventEditView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkPermissions()) {
            finish();
            showToast("On your device, allow the calendar permission");
            return;
        }
        setContentView(R.layout.activity_edit);
        setupUI(findViewById(R.id.edit_activity), this);

        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmFinish();
            }
        });

        ((Button)findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (save()) {
                    finish();
                }
            }
        });
        ((Button)findViewById(R.id.btn_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmDelete();
            }
        });
        mEventEditView = (EventEditView) findViewById(R.id.event_edit_view);
        EventEditView.Event event;
        if (savedInstanceState == null) {
            event = getIntent().getParcelableExtra(EXTRA_EVENT);
            if (event == null) {
                event = EventEditView.Event.createInstance();
            }
            //noinspection ConstantConditions
            mEventEditView.setEvent(event);
            Bundle args = new Bundle();
            args.putLong(EXTRA_CALENDAR_ID, event.getCalendarId());
            getSupportLoaderManager().initLoader(LOADER_SELECTED_CALENDAR, args, this);
        } else {
            event = savedInstanceState.getParcelable(STATE_EVENT);
            //noinspection ConstantConditions
            mEventEditView.setEvent(event);
            Log.d("title===",event.getTitle());
        }
        setTitle(event.hasId() ? R.string.edit_event : R.string.create_event);
        if (!event.hasId()){
            ((Button)findViewById(R.id.btn_delete)).setVisibility(View.GONE);
        }
        getSupportLoaderManager().initLoader(LOADER_CALENDARS, null, this);




    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_EVENT, mEventEditView.getEvent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mEventEditView != null) { // may be null if not created due to missing permissions
            mEventEditView.swapCalendarSource(null);
        }
    }

    @Override
    public void onBackPressed() {
        confirmFinish();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void setTitle(int titleId) {
        if (findViewById(R.id.tv_event_window_title) != null) { // exist in landscape
            ((TextView) findViewById(R.id.tv_event_window_title)).setText(titleId);
        } else {
            getSupportActionBar().setDisplayOptions(
                    getSupportActionBar().getDisplayOptions() | ActionBar.DISPLAY_SHOW_TITLE);
            super.setTitle(titleId);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        if (id == LOADER_SELECTED_CALENDAR) {
            selection = CalendarContract.Calendars._ID + "=?";
            selectionArgs = new String[]{String.valueOf(args.getLong(EXTRA_CALENDAR_ID))};
        }
        return new CursorLoader(this, CalendarContract.Calendars.CONTENT_URI,
                CalendarCursor.PROJECTION,
                selection, selectionArgs,
                CalendarContract.Calendars.DEFAULT_SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CALENDARS:
                if (data != null && data.moveToFirst()) {
                    mEventEditView.swapCalendarSource(new CalendarCursor(data));
                }
                break;
            case LOADER_SELECTED_CALENDAR:
                if (data != null && data.moveToFirst()) {
                    mEventEditView.setSelectedCalendar(new CalendarCursor(data).getDisplayName());
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == LOADER_CALENDARS) {
            mEventEditView.swapCalendarSource(null);
        }
    }

    @VisibleForTesting
    protected boolean checkPermissions() {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) |
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void confirmFinish() {

        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_confirm_delete, null, false);
        final androidx.appcompat.app.AlertDialog pinDialog = new androidx.appcompat.app.AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);
        TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
        TextView txvLogout = (TextView) pinDialogView.findViewById(R.id.tv_confirm);
        TextView tvTitle = (TextView) pinDialogView.findViewById(R.id.tv_alertTitle);
        tvTitle.setText("Are you sure finish?");
        txvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });
        txvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
                EditActivity.super.onBackPressed();
            }
        });
        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();
    }

    private boolean save() {
        EventEditView.Event event = mEventEditView.getEvent();
        if (!isValid(event)) {
            return false;
        }
        ContentValues cv = new ContentValues();
        cv.put(CalendarContract.Events.TITLE, event.getTitle());
        cv.put(CalendarContract.Events.DTSTART, event.getStartDateTime());
        cv.put(CalendarContract.Events.DTEND, event.getEndDateTime());
        cv.put(CalendarContract.Events.ALL_DAY, event.isAllDay());
        cv.put(CalendarContract.Events.EVENT_END_TIMEZONE, event.getTimeZone());
        cv.put(CalendarContract.Events.EVENT_TIMEZONE, event.getTimeZone());
        cv.put(CalendarContract.Events.CALENDAR_ID, event.getCalendarId());
        if (event.hasId()) {
            Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI,
                    event.getId());
            new EventQueryHandler(this)
                    .startUpdate(0, null, uri, cv, null, null);
        } else {
            new EventQueryHandler(this)
                    .startInsert(0, null, CalendarContract.Events.CONTENT_URI, cv);
        }
        return true;
    }

    private boolean isValid(EventEditView.Event event) {
        if (!event.hasCalendarId()) {
            //noinspection ConstantConditions
            Snackbar.make(findViewById(R.id.edit_activity),
                    R.string.warning_missing_calendar,
                    Snackbar.LENGTH_SHORT).setActionTextColor(getResources().getColor(R.color.colorRed))
                    .show();
            return false;
        }
        return event.hasTitle();
    }

    private void confirmDelete() {
        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_confirm_delete, null, false);
        final androidx.appcompat.app.AlertDialog pinDialog = new androidx.appcompat.app.AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);
        TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
        TextView txvLogout = (TextView) pinDialogView.findViewById(R.id.tv_confirm);
        txvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });
        txvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
                delete();
                finish();
            }
        });
        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();
    }

    private void delete() {
        new EventQueryHandler(this).startDelete(0, null,
                ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI,
                        mEventEditView.getEvent().getId()),
                null, null);
    }

    class EventQueryHandler extends AsyncQueryHandler {

        private final WeakReference<Context> mContext;

        public EventQueryHandler(Context context) {
            super(context.getContentResolver());
            mContext = new WeakReference<>(context);
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            if (mContext.get() != null) {
                showToast(getString(R.string.event_created));
            }
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            if (mContext.get() != null) {
                showToast(getString(R.string.event_updated));
            }
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            if (mContext.get() != null) {
                showToast(getString(R.string.event_deleted));
            }
        }
    }




}
