package com.dj.royaltyradio.eventactivity.widget;

import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.TextView;

import com.dj.royaltyradio.R;
import com.dj.royaltyradio.eventactivity.CalendarUtils;
import com.dj.royaltyradio.eventactivity.EditActivity;
import com.dj.royaltyradio.eventactivity.content.EventCursor;
import com.skyhope.eventcalenderlibrary.CalenderEvent;
import com.skyhope.eventcalenderlibrary.listener.CalenderDayClickListener;
import com.skyhope.eventcalenderlibrary.model.DayContainerModel;
import com.skyhope.eventcalenderlibrary.model.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class TestActivity extends AppCompatActivity {
    /*private static final int MONTH_SIZE = 31;

    @VisibleForTesting static final int BLOCK_SIZE = MONTH_SIZE;
    @VisibleForTesting
    static final int MAX_SIZE = MONTH_SIZE * 3;
    private static final String STATE_EVENT_GROUPS = "state:eventGroups";
    private final EventGroupList mEventGroups = new EventGroupList(BLOCK_SIZE);*/
    TextView txvtitle, txvdate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
         txvtitle=(TextView)findViewById(R.id.tv_event_title);
        txvdate=(TextView)findViewById(R.id.tv_event_date);

        CalenderEvent calenderEvent = findViewById(R.id.calender_event);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Event event = new Event(calendar.getTimeInMillis(), "Test");
        calenderEvent.addEvent(event);

        calendar.add(Calendar.DAY_OF_MONTH, 2);
        Event event1 = new Event(calendar.getTimeInMillis(), "New One");
        calenderEvent.addEvent(event1);

        calenderEvent.initCalderItemClickCallback(new CalenderDayClickListener() {
            @Override
            public void onGetDay(DayContainerModel dayContainerModel) {
                txvdate.setText(dayContainerModel.getDate());
                if(dayContainerModel.getEvent()!=null)
                    txvtitle.setText(dayContainerModel.getEvent().getEventText());
                else
                    txvtitle.setText("No event");

            }
        });
    }
}
