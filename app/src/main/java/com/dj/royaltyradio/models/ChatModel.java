package com.dj.royaltyradio.models;

public class ChatModel {

    private int id = 0;
    private String pictureUrl = "";
    private String title = "";
    private String username = "";
    private long tTime = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long gettTime() {
        return tTime;
    }

    public void settTime(long tTime) {
        this.tTime = tTime;
    }
}
